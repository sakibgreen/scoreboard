<?php 

Auth::routes();

Route::get('/', 'PostController@index')->name('home');
Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');
 
//Post Controller
// Route::resource('posts', 'PostController');
Route::delete('posts/{id}/forcedelete', 'PostController@forcedelete')->name('posts.forcedelete');
Route::get('posts/manage', 'PostController@manage')->name('posts.manage');
Route::get('posts/trashed', 'PostController@trashed')->name('posts.trashed');
Route::post('posts/{id}/restore', 'PostController@restore')->name('posts.restore');

Route::get('posts','PostController@index')->name('posts.index');
Route::post('posts','PostController@store')->name('posts.store');
Route::get('posts/create','PostController@create')->name('posts.create');
Route::get('posts/{post}','PostController@show')->name('posts.show');
Route::put('posts/{post}','PostController@update')->name('posts.update');
Route::get('posts/{post}/edit','PostController@edit')->name('posts.edit');
Route::delete('posts/{post}','PostController@destroy')->name('posts.destroy');

Route::put('posts/{post}/toggle-approve','PostController@approval')->name('posts.approval');


