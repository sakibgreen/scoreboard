@extends('layouts.app')

@section('title', '| Edit Post')

@section('content')
<div class="row">

    <div class="col-md-8 col-md-offset-2">

        <h1>Edit Post</h1>
        <hr>
        <form class="form-horizontal mt-5" action="{{route('posts.update', $post->id)}}" method="post" enctype="multipart/form-data">
            {{ method_field('PUT')}}
            {{csrf_field()}}

            <div class="form-group">
                <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{ $post->title}}">

                <!--Image-->
                <input type="file" name="image_name" required="required">

                <div class="form-group">
                    <label class="col-md-12" for="body">Price</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="body" id="body" placeholder="writing price" value="{{$post->body}}">
                    </div>
                </div>
               
                <label for="address">Address</label>
                    <input type="text" name="address" class="form-control" value="{{ $post->address}}">
                <label for="contact">Contact</label>
                    <input type="text" name="contact" class="form-control" value="{{ $post->contact}}">
                
                <label class="col-md-4 control-label" for=""></label>
                <button class="btn btn-info btn-block"> Save </button>
            </div>
        </form>
        
    </div>
</div>

@endsection 