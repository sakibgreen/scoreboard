@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3><center>All Users List</center></h3>
                    </div>
                    
                    <div class="row marketing">
                        @foreach ($posts as $post)
                        <div class="col-lg-3">
                            <img class="card-img-top" src="{{ asset("storage/upload/".$post->image_name)}}" width="100%" height="200" alt="Card image cap">

                            <div class="panel-body">
                                <a href="{{ route('posts.show', $post->id ) }}"><b>{{ $post->title }}</b>    <br>
                                    <p class="teaser">
                                    {{str_limit($post->body, 100) }} {{-- Limit teaser to 100 characters --}}
                                    </p>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!--Soft Delete-->
                <div class="text-center">
                    {!! $posts->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

