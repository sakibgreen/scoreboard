@extends('layouts.app')

@section('title', '| Create New Post')

@section('content')
<div class="container">
    <form class="form-horizontal mt-5" action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <fieldset>
            <div class="col-md-8 col-md-offset-2">
                <h1>Create New Post</h1>
                <!--title area-->
                <div class="form-group">
                    <label class="col-md-12" for="title">Title</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="title" id="title" placeholder="writing your title">
                    </div>
                </div>

                <!--Image-->
                <input type="file" name="image_name">

                <!--Price area-->
                <div class="form-group">
                    <label class="col-md-12" for="body">Price</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="body" id="body" placeholder="writing price">
                    </div>
                </div>
                <!--Address area-->
                <div class="form-group">
                    <label class="col-md-12" for="address">Address</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="address" id="address" placeholder="writing address">
                    </div>
                </div>
                <!--Contact area-->
                <div class="form-group">
                    <label class="col-md-12" for="contact">Contact</label>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="contact" id="contact" placeholder="writing contact">
                    </div>
                </div>
                <!--Button-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <button class="btn btn-warning btn-block"><i class="far fa-save"></i> Save </button>
                    </div>
                </div>  
            </div>
        </fieldset>
    </form>
    
</div>

@endsection
