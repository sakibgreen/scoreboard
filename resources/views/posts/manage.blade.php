@extends('layouts.app')

@section('content')

	<h1><center>Manage your Post</center></h1>
	<div class="post-meta padding-10 clearfix">
        <div class="pull-left">
            <ul class="post-meta-group">
                <i class="fa fa-user"></i> <posts>Your Total Posts: {{$posts->count()}} </posts>
            </ul>
        </div>
        <div class="pull-right">
        	<a href="{{ route('posts.trashed')}}" class="btn btn-success"><i class="fas fa-trash-alt"></i> Trashed</a>
        </div>
    </div>
    <div class="table-responsive container">        
	<table class="table table-bordered">
		<thead>
    		<tr>
    			<th scope="col">Number</th>
			    <th scope="col">Title</th>
			    <th scope="col">image</th>
			    <th scope="col">Price</th>
			    <th scope="col">Address</th> 
			    <th scope="col">Contact</th>  
				<th scope="col">Action</th>
				<th scope="col">Approve</th>

			</tr>
		</thead>
		<tbody>
			@foreach($posts as $post)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $post->title }}</td>  		
	  				<td><img src="{{ asset("storage/upload/".$post->image_name)}}" width="200" height="100"></td>
					<td>{{  str_limit($post->body, 100) }}</td>
					
					<td>{{ $post->address }}</td>  		
					<td>{{ $post->contact }}</td>  		

					<td><a href="{{route('posts.edit', $post->id)}}" class="btn btn-warning" style="display: inline;"><i class="fas fa-edit"></i> Edit</a>
					
					<form action="{{route('posts.destroy',$post->id)}}" method="post" style="display: inline;">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button>
						</form>
					</td>
					<td>
						@if(auth()->user()->hasPermissionTo('Approve Post'))
						<form action="{{route('posts.approval', $post->id)}}" method="POST">
							{{csrf_field()}}
							{{method_field('PUT')}}
							@if($post->approve == 1)
							<button class="btn btn-danger"><i class="far fa-times-circle"></i> Not Approved</button>
							@else
							<button class="btn btn-success"><i class="far fa-thumbs-up"></i> Approved</button>
							@endif
						</form>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	</div>
@endsection   