@extends('layouts.app')

@section('title', '| Create Permission')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-key'></i> Add Permission</h1>
    <br>
    <form class="form-horizontal mt-5" action="{{route('permissions.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}

        <div class="form-group">
            <label for="name">Add New Parmission</label>
            <input type="text" name="name" class="form-control"><br><br>
        </div>
        
        @if(!$roles->isEmpty()) 
            <h5>If no roles exist yet</h5>
            <h6>যদি কোনও ভূমিকা এখনো বিদ্যমান থাকে</h6>
            <h3>Assign Permission to Roles</h3>

            @foreach ($roles as $role) 
                <input type="checkbox" name="roles[]" value="{{$role->id}}">{{$role->name}}<br>
            @endforeach
        @endif
        <br>
        <button class="btn btn-primary"><i class="fas fa-plus-square"></i> Add</button>
    </form>

</div>

@endsection
