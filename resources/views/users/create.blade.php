@extends('layouts.app')

@section('title', '| Add User')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-user-plus'></i> Add User</h1>
    <hr>

    <form class="form-horizontal mt-5" action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}

    <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="">
    </div>

    <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" class="form-control" value="">
    </div>

    <div class='form-group'>
        @foreach ($roles as $role)
            <input type="checkbox" name="roles[]" value="{{$role->id}}">{{$role->name}} <br>
        @endforeach
    </div>

    <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control">
        </div>

        <div class="form-group">
            <label for="password">Confirm Password</label>
            <input type="password" name="password_confirmation" class="form-control">
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for=""></label>
            <div class="col-md-4">
                <button class="btn btn-info btn-lg btn-block"> Save </button>
            </div>
        </div>  

    </form>

</div>

@endsection