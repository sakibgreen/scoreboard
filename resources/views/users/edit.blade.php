@extends('layouts.app')

@section('title', '| Edit User')

@section('content')

<div class='col-lg-4 col-lg-offset-4'>

    <h1><i class='fa fa-user-plus'></i> Edit {{$user->name}}</h1>
    <hr>
    <form class="form-horizontal mt-5" action="{{route('users.update', $user->id)}}" method="post" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{$user->name}}">
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" class="form-control" value="{{$user->email}}">
        </div>
        
        <h5><b>Give Role</b></h5>
        <div class='form-group'>
            @foreach ($roles as $role)   
                @if($u_role_id == $role->id)
                    <input type="checkbox" name="roles[]" value="{{$role->id}}" checked="checked">{{$role->name}}<br>
                @else
                    <input type="checkbox" name="roles[]" value="{{$role->id}}">{{$role->name}}<br>
                @endif
            @endforeach     
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control">
        </div>

        <div class="form-group">
            <label for="password">Confirm Password</label>
            <input type="password" name="password_confirmation" class="form-control">
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for=""></label>
            <div class="col-md-4">
                <button class="btn btn-info btn-lg btn-block"> Add </button>
            </div>
        </div>  
    </form>

</div>

@endsection