<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{   
    use SoftDeletes;
    protected $table    = 'posts';
    protected $fillable = [ 'title','body' ];
    protected $dates    = ['deleted_at'];

    
    public function users()
    {
    	return $this->belongsTo(User::class);
    }

    public static function user_name($id)
    {
    	$name = User::find($id);
    	if($name != NULL)
    	{
    		return $name->name;
    	}
    	else
    	{
    		return 'Null';
    	}
    }
}
